######################################################################
## 'WILDseq_barcode - linker - WILDseqbarcode - linker - PrimerUMI' ## 
######################################################################

def samples_path(wildcards):
	return {sample_table.loc[wildcards.sample, "path"]}

# # trimming 3' adapters
rule cutadapt_3_prime_adaptor:
    input:
        samples_path,
    output:
        fastq="trimmed/{sample}.fq.gz",
        qc="trimmed/{sample}.qc.txt",
    params:
        adapters="-a CATGCGCTCGTTTACTATACGAT...CTGTCTCTTATACACATCTCCGAGCCCACGAGAC",
        extra="--discard-untrimmed",
    log:
        "logs/cutadapt/{sample}.log",
    threads: 4  # set desired number of threads here
    wrapper:
        "v1.21.1/bio/cutadapt/se"

####################################################################
# Filter for barcode reads and remove "--" lines in the fastq files#
##### wildseq barcode - linker - barcode - another_linker - umi ####
####################################################################
rule get_barcode_reads:
    input:
        "trimmed/{sample}.fq.gz",
    output:
        "trimmed/selected/{sample}_selected.fq",
    log:
        "logs/get_barcode_reads/{sample}.log",
    shell:
        "(zcat {input} | egrep -A 2 -B 1 "
        "^[ATGC]{{12}}TGCATCGGTTAACCGATGCA[ATGC]{{12}}CGGATAGAACTTTGAATCGCTTG[ATGC]{{8}}$ | "
        "sed '/^--$/d' > {output}) "
        "&> {log} "


# Extract primer UMIs from reads and add to read name for downstream processing
rule umitools_get_primer_UMIs:
    input:
        "trimmed/selected/{sample}_selected.fq",
    output:
        "trimmed/extracted/{sample}_trimmed_selected.UMI.fq",
    params:
        extract_method="string",
        bc_pattern="NNNNNNNN",
        extras="--3prime",
    conda:
        "../envs/umitools.yaml"
    log:
        "logs/umitools/{sample}.log"
    shell:
        "umi_tools extract --extract-method {params.extract_method} "
        "--bc-pattern {params.bc_pattern} {params.extras} "
        "--stdin {input} --stdout {output} --log={log}"

######################################################        
# trimming WILDseqbarcode - primerumi linker sequence#
######################################################
rule cutadapt_trim_linker:
    input:
        "trimmed/extracted/{sample}_trimmed_selected.UMI.fq",
    output:
        fastq="trimmed/processed/{sample}_trimmed_selected.UMI.fq",
        qc="trimmed/processed/{sample}_trimmed_selected.qc.txt",
    params:
        adapters="-a CGGATAGAACTTTGAATCGCTTG",
        extra="--discard-untrimmed",
    log:
        "logs/cutadapt/{sample}.log",
    threads: 4  # set desired number of threads here
    wrapper:
        "v1.21.1/bio/cutadapt/se"

#################################################################        
# Make df with two columns (PrimerUMIseq : WILDseqbarcodelinker)#
# currently hardcodes read names as starting with @V, may break #
#################################################################
rule awk_make_df:
    input:
        "trimmed/processed/{sample}_trimmed_selected.UMI.fq",
    output:
        "trimmed/processed/awk/{sample}_selected.txt",
    log:
        "logs/awk/{sample}.log",
    shell:
        """(awk 'BEGIN{{RS="@V"}}{{print $1,"\t",$3}}' """
        """{input} | awk -F"\t|_" '{{print $2,$3}}' """
        """| awk -e '$1 ~ /\w/ {{print $0}}'> {output}) &> {log}"""


# count primerUMI /  Barcode matching pairs and add counts to df 
rule count_primer_UMIs:
    input:
        "trimmed/processed/awk/{sample}_selected.txt",
    output:
        "results/{sample}_selected.txt",
    log:
        "logs/primer_UMI_collapse/{sample}.log",
    shell:
        """(sort {input} | uniq -c > {output}) &> {log}"""
