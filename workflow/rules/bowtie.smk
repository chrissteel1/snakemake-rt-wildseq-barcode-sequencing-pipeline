#######################################
##### Make a df with just barcodes ####
###using awk to ouput the third feild##
#######################################
rule get_barcode_only_df:
    input:
        "results/{sample}_selected.txt",
    output:
        "bowtie_twist_library/{sample}.BC.only.txt",
    log:
        "logs/get_barcode_only/{sample}.log",
    shell:
        """(awk '{{print $3}}' {input} > {output}) &> {log}"""


#########################################################################
## mapping to list of theoretical barcodes based on twist oligo library##
#########################################################################
rule bowtie_mapping:
    input:
        sample_barcodes="bowtie_twist_library/{sample}.BC.only.txt",
    output:
        "bowtie_twist_library/{sample}_Twist_library_mapped.bowtie",
    params:
        barcode_lib_prefix="Twist_barcode_library/theoretical_barcodes"
    conda:
        "../envs/bowtie.yaml"
    log:
        "logs/bowtie/{sample}.log",
    shell:
        "(bowtie -r -x {params.barcode_lib_prefix} {input.sample_barcodes} > {output}) &> {log} "

        
rule count_mapped_barcodes:
    input:
        "bowtie_twist_library/{sample}_Twist_library_mapped.bowtie",
    output:
        "final_counts/{sample}_Twist_BC_count.txt",
    log:
        "logs/count_mapped_barcodes/{sample}.log",
    shell:
        """(awk -F '\t' '{{print $3}}' {input} | sort | uniq -c > {output}) &> {log}"""
