################################################################
## script to run pipeline on cambridge university HPC (SLURM) ##
################################################################

export TMPDIR=/home/cjs236/rds/rds-burkitt-lymphoma-cyiwgCzJok8/snakemake/temp/ && \
       mkdir slurm_logs && \ 
	nohup snakemake --cores 56 \
      --jobs 12 --use-conda \
      --conda-frontend conda \
      -k --rerun-incomplete \
      --cluster "sbatch -c 20 -p cclake --time=12:00:00  --output=/home/cjs236/rds/rds-burkitt-lymphoma-cyiwgCzJok8/WILDseq/white_list/slurm_logs/myjob_%j.out" &
